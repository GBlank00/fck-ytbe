import logging
import os
import ffmpeg
import pyfiglet
from tqdm import tqdm
from pytube import YouTube

LOGLEVEL = os.environ.get("LOGLEVEL", "INFO").upper()
logging.basicConfig(level=LOGLEVEL)
logger = logging.getLogger(__name__)


def main():

    try:
        ASCII_art_1 = pyfiglet.figlet_format("Fuck YouTube")
        print(ASCII_art_1)

        link = input("Insert a YouTube video link to download: ")
        yt = YouTube(link)
    except Exception as e:
        logger.error(f"An error occurred: {e}")
        exit()

    print(f"Title: {yt.title}")
    print(f"Author: {yt.author} | Duration: {yt.length} seconds")

    def download_progress(stream, chunk, file_handle, bytes_remaining):
        contentSize = stream.filesize
        pbar = tqdm(total=contentSize, unit="B", unit_scale=True)
        pbar.update(contentSize - bytes_remaining)

    def download_audio(yt):
        audio_file = yt
        print(f"\n[x] Downloading: {audio_file.title}")
        audio_file.streams.filter(only_audio=True).first().download(
            "YT-Downloads/audio/"
        )
        download_progress(
            yt.streams.filter(only_audio=True).first(),
            None,
            None,
            0,
        )
        print("[x] Audio file download complete.")

    def download_video(yt):
        print(f"\n[x] Downloading: {yt.title}.mp4")
        yt.streams.filter(adaptive=True, file_extension="mp4").first().download(
            "YT-Downloads/video/"
        )
        download_progress(
            yt.streams.filter(adaptive=True, file_extension="mp4").first(),
            None,
            None,
            0,
        )

        download_audio(yt)

        # Merge audio and video
        print("[x] Merging audio and video...")
        video = ffmpeg.input(f"YT-Downloads/video/{yt.title}.mp4")
        audio = ffmpeg.input(f"YT-Downloads/audio/{yt.title}.mp4")
        ffmpeg.output(video, audio, f"YT-Downloads/{yt.title}.mp4").run(
            overwrite_output=True
        )
        print("[x] Video file download complete.")

    try:
        download_option = input("Download video or audio? (v/a): ")
        if download_option == "v":
            download_video(yt)
        elif download_option == "a":
            download_audio(yt)
        else:
            print("Invalid option. Please provide a valid option.")
            exit()
    except Exception as e:
        logger.error(f"An error occurred: {e}")
        exit()


if __name__ == "__main__":
    main()
